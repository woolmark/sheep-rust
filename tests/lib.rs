
extern crate sheep;

#[test]
fn frame_scale1() {
    assert_eq!(sheep::Frame::new(120, 120).scale, 1);
}

#[test]
fn frame_scale2() {
    assert_eq!(sheep::Frame::new(300, 260).scale, 2);
}

#[test]
fn frame_width() {
    assert_eq!(sheep::Frame::new(120, 120).width, 120);
}

#[test]
fn frame_height() {
    assert_eq!(sheep::Frame::new(120, 120).height, 120);
}


