# sheep for Rust

sheep for Rust is an implementation [sheep] with [Rust].
That does not include GUI, only includes the algrorithm.

# How to use

This repository is based on `cargo` build system.
To use, run the `cargo` commands.

    % cargo build
    % cargo run

# License
[WTFPL]

[sheep]: https://woolmark.bitbucket.org/ "Sheep"
[Rust]: https://www.rust-lang.org "Rust"
[WTFPL]: http://www.wtfpl.net "WTFPL"

