
#ifndef _SHEEP_RUST_H__
#define _SHEEP_RUST_H__

#define HAS_SIZE \
    int32_t x; \
    int32_t y; \
    int32_t width; \
    int32_t height;

typedef struct {
    HAS_SIZE
} sheep_rust_size;

typedef struct {
    HAS_SIZE
    int32_t jump_state;
    int32_t stretch;
} sheep_rust_sheep;

typedef struct sheep_rust_sheep_flock_s sheep_rust_sheep_flock;


extern sheep_rust_size sheep_rust_get_frame_size(int width, int height);
extern sheep_rust_size sheep_rust_get_ground_size(int width, int height);
extern sheep_rust_size sheep_rust_get_fence_size(int width, int height);

extern sheep_rust_sheep_flock *sheep_rust_new_sheep_flock();
extern void sheep_rust_free_sheep_flock();

extern sheep_rust_sheep_flock *sheep_rust_push_new_sheep(sheep_rust_sheep_flock *, int, int);
extern sheep_rust_sheep_flock *sheep_rust_update_sheep_flock(sheep_rust_sheep_flock *);

extern int sheep_rust_get_sheep_count(sheep_rust_sheep_flock *);
extern sheep_rust_sheep sheep_rust_get_sheep_at(sheep_rust_sheep_flock *, int);

#endif /* _SHEEP_RUST_H__ */

