
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include <sheep_rust.h>

static const int FRAME_WIDTH = 120;
static const int FRAME_HEIGHT = 120;

int main() {

    // Create the area
    // The following sizes are for debug

    sheep_rust_area frame_area = sheep_rust_get_frame_area(FRAME_WIDTH, FRAME_HEIGHT);
    printf("frame area: %d, %d, %d, %d\n", frame_area.x, frame_area.y, frame_area.width, frame_area.height);

    sheep_rust_area ground_area = sheep_rust_get_ground_area(FRAME_WIDTH, FRAME_HEIGHT);
    printf("ground area: %d, %d, %d, %d\n", ground_area.x, ground_area.y, ground_area.width, ground_area.height);

    sheep_rust_area fence_area = sheep_rust_get_fence_area(FRAME_WIDTH, FRAME_HEIGHT);
    printf("fence area: %d, %d, %d, %d\n", fence_area.x, fence_area.y, fence_area.width, fence_area.height);

    // Initialize the sheep flock
    sheep_rust_sheep_flock *flock = sheep_rust_new_sheep_flock();

    // Add a sheep into the sheep flock
    flock = sheep_rust_push_new_sheep(flock, FRAME_WIDTH, FRAME_HEIGHT);

    // Run a sheep if a sheep is in the frame
    while (sheep_rust_get_sheep_count(flock) > 0) {

        sheep_rust_sheep sheep = sheep_rust_get_sheep_at(flock, 0);
        printf("sheep: %d, %d, %d\n", sheep.x, sheep.y, sheep.stretch);

        flock = sheep_rust_update_sheep_flock(flock);

        usleep(100 * 1000); // 100msec
    }

    // Finalize the sheep flock
    sheep_rust_free_sheep_flock(flock);

    return 0;
}

