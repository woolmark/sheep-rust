use std::mem;

use Frame;
use Sheep;
use Size;
use HasSize;
use SheepFlock;
use new_sheep_flock;
use update_sheep_flock;
use push_new_sheep;
use get_jumped_sheep_count;

/// Size for c lang.
#[repr(C)]
pub struct CArea {
    x: i32,
    y: i32,
    width: i32,
    height: i32,
}

/// Sheep for c lang.
#[repr(C)]
pub struct CSheep {
    x: i32,
    y: i32,
    width: i32,
    height: i32,
    stretch: i32,
}

impl From<Size> for CArea {
    fn from(size: Size) -> CArea {
        CArea { x: size.0, y: size.1, width: size.2, height: size.3 }
    }
}

impl CSheep {
    fn new(sheep: &Sheep) -> CSheep {
        let size = sheep.size();
        CSheep{x: size.0, y: size.1,
            width: size.2, height: size.3,
            stretch: match sheep.stretch {
                true => {1},
                _ => {0},
            }}
    }
}


//
// Frame, ground and fence area.
//

#[no_mangle]
pub extern fn sheep_rust_get_frame_area(width: i32, height: i32) -> CArea {
    Frame::new(width, height).size().into()
}

#[no_mangle]
pub extern fn sheep_rust_get_ground_area(width: i32, height: i32) -> CArea {
    Frame::new(width, height).ground_size().into()
}

#[no_mangle]
pub extern fn sheep_rust_get_fence_area(width: i32, height: i32) -> CArea {
    Frame::new(width, height).fence_size().into()
}

//
// Managing sheep flock and sheep
//

/// Create new sheep flock.
#[no_mangle]
pub extern fn sheep_rust_new_sheep_flock() -> *const SheepFlock {
    unsafe {
        mem::transmute(Box::new(new_sheep_flock()))
    }
}

/// Free sheep flock.
#[no_mangle]
pub extern fn sheep_rust_free_sheep_flock(flock_ptr: *const SheepFlock) {

    if flock_ptr.is_null() { return }
    let _: Box<SheepFlock> = unsafe {
        mem::transmute(flock_ptr)
    };

}

/// Update sheep flock with a tick.
#[no_mangle]
pub extern fn sheep_rust_update_sheep_flock(flock_ptr: *const SheepFlock) -> *const SheepFlock {

    let flock: Box<SheepFlock> = unsafe {
        assert!(!flock_ptr.is_null());
        mem::transmute(flock_ptr)
    };

    let updated_flock = update_sheep_flock(*flock);

    unsafe {
        mem::transmute(Box::new(updated_flock))
    }

}

/// Push a sheep into sheep flock.
#[no_mangle]
pub extern fn sheep_rust_push_new_sheep(flock_ptr: *const SheepFlock, width: i32, height: i32) -> *const SheepFlock {

    let flock: Box<SheepFlock> = unsafe {
        assert!(!flock_ptr.is_null());
        mem::transmute(flock_ptr)
    };

    let frame = Frame::new(width, height);
    let pushed_flock = push_new_sheep(*flock, &frame);

    unsafe {
        mem::transmute(Box::new(pushed_flock))
    }

}

#[no_mangle]
pub extern fn sheep_rust_get_sheep_count(flock_ptr: *const SheepFlock) -> i32 {
    let flock = unsafe {
        assert!(!flock_ptr.is_null());
        &*flock_ptr
    };

    flock.len() as i32
}

#[no_mangle]
pub extern fn sheep_rust_get_jumped_sheep_count(flock_ptr: *const SheepFlock) -> i32 {
    let flock = unsafe {
        assert!(!flock_ptr.is_null());
        &*flock_ptr
    };

    get_jumped_sheep_count(&flock)
}

/// Get a sheep from sheep flock.
#[no_mangle]
pub extern fn sheep_rust_get_sheep_at(flock_ptr: *const SheepFlock, index: i32) -> CSheep {

    let flock = unsafe {
        assert!(!flock_ptr.is_null());
        &*flock_ptr
    };

    let sheep = &(flock[index as usize]);

    CSheep::new(sheep)

}