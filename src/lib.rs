extern crate rand;

use std::fmt;
use rand::Rng;

pub mod ffi;

const SHEEP_WIDTH: i32 = 12;
const SHEEP_HEIGHT: i32 = 8;

const FENCE_WIDTH: i32 = 30;
const FENCE_HEIGHT: i32 = 40;

const DEFAULT_WINDOW_WIDTH: i32 = 120;
const DEFAULT_WINDOW_HEIGHT: i32 = 120;

/// A flock of sheep.
pub type SheepFlock = Vec<Sheep>;

/// Creates new sheep flock as a vector contains empty `Sheep`.
pub fn new_sheep_flock() -> SheepFlock {
    Vec::new()
}

/// Updates the sheep flock.
pub fn update_sheep_flock(sheep_flock: SheepFlock) -> SheepFlock {
    let mut moved_sheep_flock = new_sheep_flock();

    for sheep in sheep_flock {
        let moved_sheep = move_sheep(sheep);

        if moved_sheep.is_in_frame() {
            moved_sheep_flock.push(moved_sheep);
        }
    }

    moved_sheep_flock
}

/// Pushes a new sheep into sheep flock.
pub fn push_new_sheep(sheep_flock: SheepFlock, frame: &Frame) -> SheepFlock {
    let mut sheep_flock_mut = sheep_flock;
    sheep_flock_mut.push(Sheep::new(frame));

    sheep_flock_mut
}

pub fn get_jumped_sheep_count(sheep_flock: &SheepFlock) -> i32 {
    let mut count = 0;

    for sheep in sheep_flock {
        match sheep.jump_state {
            4 => { count += 1 },
            _ => {},
        };
    }

    count
}

/// Size, with x, y, width and height.
///
/// - .0: x position
/// - .1: y position
/// - .2: width
/// - .3: height
pub type Size = (i32, i32, i32, i32);

/// A trait that represents the size.
pub trait HasSize {
    /// Returns size.
    fn size(&self) -> Size;
}


/// A sheep that represents thats position and size.
pub struct Sheep {
    x: i32,
    y: i32,
    scale: i32,
    jump_x: i32,
    pub jump_state: i32,
    pub stretch: bool,
}

impl Sheep {
    /// Creats new sheep in frame.
    pub fn new(frame: &Frame) -> Sheep {
        // .0:x, .1:y, .2:width, .3:height
        let frame_size = frame.size();
        let ground_size = frame.ground_size();
        let fence_size = frame.fence_size();

        let x = frame_size.2;
        let y = rand::thread_rng().gen_range(ground_size.1, frame_size.3) - SHEEP_HEIGHT;
        Sheep {
            x: x, y: y, scale: frame.scale,
            jump_x: -1 * (y - frame_size.3) * fence_size.2 / fence_size.3 + (frame_size.2 - fence_size.2) / 2,
            jump_state: 0,
            stretch: false
        }
    }

    /// Returns weather a sheep in the frame or not.
    pub fn is_in_frame(&self) -> bool {
        self.x > -SHEEP_WIDTH * self.scale
    }

    pub fn is_starting_jump(&self) -> bool {
        if self.x < self.jump_x && self.jump_state == 0 {
            true
        } else {
            false
        }
    }
}

impl HasSize for Sheep {
    fn size(&self) -> Size {
        (self.x, self.y, SHEEP_WIDTH * self.scale, SHEEP_HEIGHT * self.scale)
    }
}

impl fmt::Display for Sheep {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "x:{}, y:{}, stretch:{}", self.x, self.y, self.stretch)
    }
}

/// Moves a sheep.
pub fn move_sheep(sheep: Sheep) -> Sheep {
    let mut sheep_mut = sheep;

    sheep_mut.x -= 5 * sheep_mut.scale;

    if sheep_mut.is_starting_jump() {
        sheep_mut.jump_state = 1;
        sheep_mut.stretch = true;
    }

    match sheep_mut.jump_state {
        1 ... 3 => {
            sheep_mut.y -= 3 * sheep_mut.scale;
            sheep_mut.jump_state += 1
        },
        4 ... 6 => {
            sheep_mut.y += 3 * sheep_mut.scale;
            sheep_mut.jump_state += 1
        },
        _ => {
            sheep_mut.stretch = !sheep_mut.stretch
        }
    };

    sheep_mut
}

/// A frame that includes the areas of frame, ground and fence.
pub struct Frame {
    /// Scale.
    pub scale: i32,
    /// Width of frame.
    width: i32,
    /// Hidth of frame.
    height: i32,
}

impl Frame {
    /// Creats new frame with width and height.
    pub fn new(width: i32, height: i32) -> Frame {
        Frame { scale: scale(width, height), width: width, height: height }
    }

    /// Returns fence size.
    pub fn fence_size(&self) -> Size {
        let w = FENCE_WIDTH * self.scale;
        let h = FENCE_HEIGHT * self.scale;

        ((self.width - w) / 2, self.height - h, w, h)
    }

    /// Returns ground size.
    pub fn ground_size(&self) -> Size {
        let height = (self.fence_size().3 as f32 * 0.9f32) as i32;
        (0, self.height - height, self.width, height)
    }
}

impl HasSize for Frame {
    fn size(&self) -> Size {
        (0, 0, self.width, self.height)
    }
}

impl fmt::Display for Frame {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "width:{}, height:{}, scale:{}", self.width, self.height, self.scale)
    }
}

fn scale(width: i32, height: i32) -> i32 {
    let scale = match width > height {
        true => height / DEFAULT_WINDOW_HEIGHT,
        false => width / DEFAULT_WINDOW_WIDTH
    };

    match scale {
        0 => 1,
        _ => scale
    }
}