extern crate sheep;
use std::thread;
use std::time;

fn main() {

    // initailzie the frame and sheep flock
    let frame = sheep::Frame::new(120, 120);
    let mut sheep_flock = sheep::new_sheep_flock();

    // push a sheep for debug
    sheep_flock = sheep::push_new_sheep(sheep_flock, &frame);

    // loop until all sheep are in the frame
    let child = thread::spawn(move || {
        loop {
            sheep_flock = sheep::update_sheep_flock(sheep_flock);
            if sheep_flock.is_empty() {
                break;
            }

            println!("{}", sheep_flock[0]);
            thread::sleep(time::Duration::from_millis(100));
        }
    });

    child.join().unwrap();

}