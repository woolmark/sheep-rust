
#ifndef _SHEEP_RUST_H__
#define _SHEEP_RUST_H__

#include <stdint.h>

#define HAS_AREA \
    int32_t x; \
    int32_t y; \
    int32_t width; \
    int32_t height;

typedef struct {
    HAS_AREA
} sheep_rust_area;

typedef struct {
    HAS_AREA
    int32_t stretch;
} sheep_rust_sheep;

typedef struct {} sheep_rust_sheep_flock;


extern sheep_rust_area sheep_rust_get_frame_area(int32_t, int32_t);
extern sheep_rust_area sheep_rust_get_ground_area(int32_t, int32_t);
extern sheep_rust_area sheep_rust_get_fence_area(int32_t, int32_t);

extern sheep_rust_sheep_flock const *sheep_rust_new_sheep_flock();
extern void sheep_rust_free_sheep_flock(sheep_rust_sheep_flock const *);

extern sheep_rust_sheep_flock const *sheep_rust_push_new_sheep(sheep_rust_sheep_flock const *, int32_t, int32_t);
extern sheep_rust_sheep_flock const *sheep_rust_update_sheep_flock(sheep_rust_sheep_flock const *);

extern int32_t sheep_rust_get_sheep_count(sheep_rust_sheep_flock const *);
extern sheep_rust_sheep sheep_rust_get_sheep_at(sheep_rust_sheep_flock const *, int32_t);
extern int32_t sheep_rust_get_jumped_sheep_count(sheep_rust_sheep_flock const *);

#endif /* _SHEEP_RUST_H__ */

